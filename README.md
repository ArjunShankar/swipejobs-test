To run
1) npm i
2) ng serve
3) goto http://localhost:4200

- Deployed at https://swipejobs-test.firebaseapp.com
- CI/CD with Gitlab and firebase
- ServiceWorker included. Prefetches the assets. Manifest file included. 'Installable' on mobile devices.
