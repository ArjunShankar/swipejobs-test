import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserProfileComponent } from './components/user-profile/user-profile.component';


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: UserProfileComponent }
    ])
  ]
})

export class ProfileRoutingModule { }
