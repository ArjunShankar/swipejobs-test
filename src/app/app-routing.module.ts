import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path : '', redirectTo : '/search', pathMatch: 'full' },
  { path: 'search', loadChildren: './search/search.module#SearchModule', data: { preload: true }},  //todo remove lazy loading as this is landing page
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule', data: { preload: true }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
