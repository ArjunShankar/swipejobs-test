import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: LayoutComponent }
    ])
  ]
})

export class SearchRoutingModule { }
