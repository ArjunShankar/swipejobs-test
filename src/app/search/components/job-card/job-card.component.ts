import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Job} from '../../../core/interfaces/interfaces';

@Component({
  selector: 'app-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.scss']
})
export class JobCardComponent implements OnInit {

  @Input() job: Job;

  @Input() jobIndex: any;

  @Output() dismissJob: EventEmitter<number>;

  constructor() {
    this.dismissJob = new EventEmitter();
  }

  ngOnInit() {

  }

  findAddress(job: Job) {
    const url = `https://maps.google.com/?q=${job.company.address}`;
    window.open(url, '_blank');
  }

  dismissHandler(event) {
    //this.dismissJob.emit(this.jobIndex);
  }

}
