import {Component, OnDestroy, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map} from 'rxjs/operators';
import {JobSearchService} from '../../../core/services/job-search.service';
import {Subscription} from 'rxjs';
import {Job} from '../../../core/interfaces/interfaces';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  jobSubscription: Subscription;
  dataProvider: Array<Job>;

  constructor(
    private jobSearchService: JobSearchService
  ) {
  }

  ngOnInit() {
    this.jobSubscription = this.jobSearchService.getData().subscribe((result) => {
      this.dataProvider = result;
    });
  }

  ngOnDestroy() {
    if (this.jobSubscription) {
      this.jobSubscription.unsubscribe();
    }
  }

  dismissJobHandler(input) {
    this.dataProvider = this.dataProvider.slice(0, input);
  }

}
