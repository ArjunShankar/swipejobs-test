import {Component, Input, OnInit} from '@angular/core';
import {Shifts} from '../../../core/interfaces/interfaces';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-job-dates',
  templateUrl: './job-dates.component.html',
  styleUrls: ['./job-dates.component.scss']
})
export class JobDatesComponent implements OnInit {

  @Input()
  shifts: Array<Shifts>;
  timezone = 'Australia/Sydney';

  dataProvider: Array<string>;

  constructor() {
    this.dataProvider = [];
  }

  ngOnInit() {
    if (this.shifts != null && this.shifts.length > 0 ) {
      for (const shift of this.shifts) {
        this.dataProvider.push(this.formatShift(shift));
      }
    }
  }

  formatShift(shift: Shifts) {
    let toReturn = '';
    if (shift.startDate != null && moment(shift.startDate).isValid()) {
      toReturn = `${moment(shift.startDate).tz(this.timezone).format('dddd, DD MMMM h:mm:ss a z')}`;
    }
    return toReturn;
  }

}
