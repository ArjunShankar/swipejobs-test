import {Component, Input, OnInit} from '@angular/core';
import {Shifts} from '../../../core/interfaces/interfaces';
import * as moment from 'moment-mini-ts';

@Component({
  selector: 'app-date-range',
  templateUrl: './date-range.component.html',
  styleUrls: ['./date-range.component.scss']
})
export class DateRangeComponent implements OnInit {

  @Input()
  range: Array<Shifts>;

  rangeLabel: string;

  constructor() {
    this.rangeLabel = '';
  }

  ngOnInit() {

    if (this.range != null && this.range.length > 0) {
      if (this.range.length > 4) {
        const startDate = this.range[0].startDate;
        const endDate = this.range[this.range.length - 1].startDate;
        if (moment(startDate).isValid() && moment(endDate).isValid()) {
          this.rangeLabel = `${moment(startDate).format('dddd, D MMM')} - ${moment(endDate).format('dddd, D MMM')}`;
        }
      }
      else {
        for (const shift of this.range) {
          if (moment(shift.startDate).isValid()) {
              this.rangeLabel += moment(shift.startDate).format('ddd D MMM') + ', ';
          }
        }

        this.rangeLabel = this.rangeLabel.slice(0, -2);
      }
    }
  }

}
