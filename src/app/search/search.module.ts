import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { JobCardComponent } from './components/job-card/job-card.component';
import { SearchRoutingModule } from './search-routing.module';
import {MatCardModule, MatGridListModule, MatMenuModule, MatIconModule, MatButtonModule, MatListModule} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { JobDatesComponent } from './components/job-dates/job-dates.component';
import {SharedModule} from '../shared/shared.module';
import { DateRangeComponent } from './components/date-range/date-range.component';

@NgModule({
  declarations: [LayoutComponent, JobCardComponent, JobDatesComponent, DateRangeComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    MatCardModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    MatListModule,
    SharedModule
  ]
})
export class SearchModule { }
