import { CentToDollarPipe } from './cent-to-dollar.pipe';

describe('CentToDollarPipe', () => {

  let pipe;

  beforeAll(() => {
    pipe = new CentToDollarPipe();
  });

  it('create an instance', () => {
    pipe = new CentToDollarPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform correctly', () => {
    expect(pipe.transform('815', '')).toBe('8.15');
  });

  it('should transform correctly', () => {
    expect(pipe.transform('0', '')).toBe('0.00');
  });

  it('should transform correctly', () => {
    expect(pipe.transform('300', '')).toBe('3.00');
  });

});
