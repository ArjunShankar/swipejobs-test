import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'centToDollar'
})
export class CentToDollarPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const cents = parseInt(value, 10)
    const dollars   = Math.floor(cents / 100);
    const change = Math.floor(cents % 100);
    return `${dollars}.${change}`;
  }

}
