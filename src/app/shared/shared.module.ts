import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CentToDollarPipe } from './pipes/cent-to-dollar.pipe';

@NgModule({
  declarations: [CentToDollarPipe],
  exports: [
    CentToDollarPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
