import { Injectable } from '@angular/core';
import {of} from 'rxjs';
const data: any = require('../../../assets/dummyData/dummyData.json');

@Injectable({
  providedIn: 'root'
})
export class JobSearchService {

  constructor() { }

  getData() {
    return of(data);    // returns an observable .. just loading dummy data
  }

}
