export interface Job {
  jobId: string;
  title: string;
  company: Company;
  wagePerHourInCents: number;
  shifts: Array<Shifts>;
  branch: string;
  branchPhoneNumber: string;
}

export interface Company {
  name: string;
  address: string;
  logo: string;
  reportTo: string;
}

export interface Shifts {
  startDate: string;
  endDate: string;
}
