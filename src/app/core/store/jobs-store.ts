import { Job } from '../interfaces/interfaces';

export interface JobsStore {
  jobs: Job[];
}
