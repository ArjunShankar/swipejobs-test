import {Component, ElementRef, ViewChild} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent {

  links: Array<any>;
  currentPage: string;
  isScreenSmall: boolean;

//  @ViewChild('drawer') drawer: ElementRef;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => {
        this.isScreenSmall = result.matches;
        return result.matches;
      })
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router) {
    this.links = [
      {
        route: '/search', label: 'Search', icon: 'search'
      },
      {
        route: '/profile', label: 'User Profile', icon: 'perm_identity'
      }
    ];
    this.currentPage = 'Search';
    this.isScreenSmall = false;
  }

  navClick(evt, item) {
    this.router.navigate([item.route]);
    this.currentPage = item.label;
  }

}
